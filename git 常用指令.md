```
Git 常用操作

摘取代码
https://blog.csdn.net/weixin_44349418/article/details/125147905

生成秘钥
ssh-keygen -t rsa -C "你的邮箱" -f copy_id_rsa

ssh-keygen -t rsa -C dyp01430987@antgroup.com -f  ~.ssh/ant_id_rsa

将ssh添加
ssh-add ~/.ssh/id_rsa

查看是否链接成功
ssh -T git@code.alipay.com

ssh -T -p 443 git@code.alipay.com


撤销本地commit
git reset --soft HEAD~1

获取本地与远程分支的对应关系
git remote show origin 

跳过代码检查
git commit –no-verify -m””

将代码保存在暂存区
git stash 

将暂存区代码恢复，并删除暂存区代码
git stash pop 

将暂存区代码恢复，保留暂存区代码
git stash apply 

恢复指定的暂存代码
git stash apply stash@{1}

取消merge 状态
git merge --abort

# 由于你拉取pull分支前，进行过merge合并更新分支操作，而其他人在你之前已经push过一个版本，导致版本不一致
git config pull.rebase false

# git 获取某个commid 信息
git log --pretty=format:“%s” b29b8b608b4d00f85b5d08663120b286ea657b4a -1
参考：https://www.cnblogs.com/ruiy/p/15904295.html

# 修改 git log 时间格式
git config --global log.date iso  （relative|local|default|iso|rfc|short|raw）



将代码转移到另一个代码库
# 添加 B 仓库远程地址
 git remote add bOrigin git://gitUrl

# 拉取B 仓库最新代码
git fetch bOrigin

# 摘取单个 commid 
git cherry-pick <commidHash>

# 摘取多个
git cherry-pick <commidHash> <commidHash>

# 摘取一系列提交,AHash 要早于 BHash （以下提交比包含 AHash）

git cherry-pick <commidAHash>..<commidBHash>

# 摘取一系列提交 （包含AHash）
git cherry-pick <commidAHash>^..<commidBHash>

# 代码冲突，cherry-pick会停止让用户决定如何继续操作

# 解决冲突后继续执行下一个
 git cherry-pick --continue

# 发生代码冲突后，放弃合并，回到操作前的样子
git cherry-pick --abort

# 发生代码冲突后，退出 Cherry pick，但是不回到操作前的样子。
git cherry-pick —quit

# 发生代码冲突后，将引起冲突的 commit 丢弃掉
git cherry-pick —skip

跨仓库摘取实践：
1. 摘取已合并的分支节点
2. git cherry-pick -m 1 <commitHash>(需要指定父节点)
3. 解决冲突（注意冲突内容对比，如果文件路径不正确注意找到正确路径的文件再进行修改）
4. git cherry-pick --continue
5. 提交代码


```