## H5遇到问题

#### **ios父元素设置overflow-y: auto; 子元素无法滚动**

* 父元素尽量不脱离文档流；
* 设置定位的子元素不要放入父元素中；
*   给子元素设置一个min-height，值比父元素的高度大，撑开父元素。

#### **ios父元素设置overflow-y: auto; 滚动不流畅**

* -webkit-overflow-scrolling : touch;

#### **腾讯地图 H5选点组件，内嵌方法 定位不准确**

#### **ios中input执行focus方法后无法调起键盘的解决办法**

* 在点击的按钮上同时绑定touchstart方法和click方法，touchstart方法中执行input显示的命令，click中执行input.focus() 的命令，完美的解决，ios和Android都可以正常的唤起键盘。

#### **ios input或textarea失去焦点后页面不回弹，解决方案**

* 失去焦点时blur，设置 document.body.scrollTop = 0;document.documentElement.scrollTop = 0;

#### **移动端调起相册 <input type="file" accept="image/*"/>**

* 调用打开摄像头后，聚焦后拍照，点击确认，这时页面会出现刷新动作，然后回退到网站一开始进入的首页，同时localstorage,session,cookie保存的内容都会被清空。
  （暂时无解）

#### **公众号开发流程** 

* 设置-公众号设置，进行微信认证
* 开发-基本配置，向后台提供AppID与AppSecret,Ip白名单配置服务器Ip
* 开发者中心-web开发者工具，绑定开发人员（开发人员需关注公众号）
* 设置-公众号设置-功能设置，绑定js接口安全域名（前端页面域名，用于前端使用微信开放api）

#### **公众号分享问题**

* 分享调试报：config:require subscribe,  需要检查是否关注公众号

#### **音频问题**

* ios无法播放amr格式语音
* 微信公众号中ios无法录音、安卓可以
* 调用微信sdk解决IOS/Android录音，但微信录音上传至服务的是amr格式，需要后端服务器转换格式为mp3

#### **视频问题**

* HLS流可以直接打开，rtmp不可以

* 视频需要手动点击，不能自动播放

* vue-video-player 使用：

  ```vue
    <videoPlayer
          id ="videoPlayer"
          ref="videoPlayer"
          v-if="videoState"
          class="vjs-custom-skin video-c-p" 
          :options="playerOptions"
          @ready="playerReadied"
          @play="onPlayerPlay($event)"
          @pause="onPlayerPause($event)"></videoPlayer>
  ```

  ```javascript
  // 引入依赖
  import { videoPlayer } from 'vue-video-player'
  import videojs from 'video.js'
  import 'videojs-contrib-hls'
  import 'video.js/dist/video-js.css'
  import 'vue-video-player/src/custom-theme.css'
  // 注册组件
  components: {
      videoPlayer
  },
  //基本配置
  playerOptions: {
      playbackRates: [0.7, 1.0, 1.5, 2.0], //播放速度
      autoplay: true, //如果true,浏览器准备好时开始回放。
      controls: true, //控制条
      preload: "auto", //视频预加载
      muted: false, //默认情况下将会消除任何音频。
      loop: true, //导致视频一结束就重新开始。
      html5:{hls:{widthCredentials:false}},
      flash: { hls: { withCredentials: false }},
      language: "zh-CN",
      aspectRatio: "16:9", // 将播放器置于流畅模式，并在计算播放器的动态大小时使用该值。值应该代表一个比例 - 用冒号分隔的两个数字（例如"16:9"或"4:3"）
      fluid: true, // 当true时，Video.js player将拥有流体大小。换句话说，它将按比例缩放以适应其容器。
      sources: [
          {
              withCredentials: false,
              type: "application/x-mpegURL",
              src: ""
          }
      ],
      hls: true,
      width: document.documentElement.clientWidth,
      notSupportedMessage: "此视频暂无法播放，请稍后再试", //允许覆盖Video.js无法播放媒体源时显示的默认信息。
      controlBar: {
          timeDivider: true,
          durationDisplay: true,
          remainingTimeDisplay: false,
          fullscreenToggle: true // 全屏按钮
      }
  
  }
  //动态更改HLS源
  this.playerOptions.sources[0].src = res[0].hls;
  ```
* 基础动画视频配置
```javascript
<div className={styles.videoContainer}>
  <img // 视频加载失败时的兜底图片展示
    className={styles.poster}
    style={{ opacity: _state === VideoState.PLAYING ? '0' : '1' }}
    src={poster}
    alt=""
  />
  <video
    onContextMenu={(e)=>{
      e.preventDefault()
    }} // 禁止右键打开菜单
    loop // 循环播放
    muted // 必须要设置为静音，否则浏览器会禁止自动播放
    autoPlay // 自动播放
    playsInline // 内联播放，否则会有样式问题
    crossOrigin="anonymous"
    controlsList="nodownload" // 禁止下载
    disableRemotePlayback
    disablePictureInPicture
    x-webkit-airplay="deny" // 禁止 airplay
    ref={videoRef}
    style={style}
    poster={poster} // 缩略图
    className={classnames(styles.video, { [styles[mixBlendMode || '']]: mixBlendMode })}
  >
    {video?.webm ? <source src={video.webm} type="video/WebM" /> : null}
    <source src={video.mp4} type="video/MP4" />
  </video>
</div>
```

#### **ios禁用长按出现放大镜、搜索、复制、粘贴/安卓同样适用**

* 点击的标签或body设置

  ```css
  body{
   	-webkit-touch-callout:none; 
      -webkit-user-select:none; 
      -moz-user-select:none;
      -ms-user-select:none;
      user-select:none;   
  }
  // ios会导致所有的输入框自动失去焦点无法输入内容，解决方案：
  input , textarea{
    -webkit-user-select:auto;
  }
  ```

* 点击的标签内设置一个透明遮罩

#### **微信公众号开发，授权之后在部分安卓手机上显示空白**

* 安卓机的微信x5内核默认使用http2代理，所以会导致http的接口无法调通；
*  http://debugx5.qq.com 可以打开微信设置
* 使用https解决

#### **微信、H5调用手机定位**


*  使用 navigator.geolocation.getCurrentPosition
```
	// H5 获取当前位置经纬度
    var location_lon = '',location_lat = ''; // 经度,纬度
    if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(function (position) {
            location_lon = position.coords.longitude;
            location_lat = position.coords.latitude;
           // alert('h5经度：'+location_lon);alert('h5纬度：'+location_lat);
        });
    }else {
        alert("您的设备不支持定位功能");
    }
    注：一定要使用https,才可以生效
```
* 使用微信jsdk内置定位

```
wx.config({
    debug: true, 
    appId: '', // 必填，公众号的唯一标识
    timestamp: , // 必填，生成签名的时间戳
    nonceStr: '', // 必填，生成签名的随机串
    signature: '',// 必填，签名
    jsApiList: ['getLocation'] // 必填，需要使用的JS接口列表
});
```

### **关于手机端input获取焦点呼起键盘背景图片挤压解决方案**
* 将背景图片height: 100%改为min-height:100%即可



## 小程序遇到的问题 

#### **微信小程序登录问题--第一次登录失败刷新再次登录成功问题**

* 出现这样问题都是先获取用户信息wx.getUserInfo（）encryptedData与iv，再进行登录wx.login（）获取code，code是用来生成session_key用来解密encryptedData与iv的。
* 所以等你先获取用户encryptedData与iv时再获取code时session_key会与encryptedData解密出来的session_key不同，每次获取code就会产生一个新的session_key，就会失败了，则只能在获取code后获取encryptedData。正确做法是先wx.login（）成功后再wx.getUserInfo（）。

#### **js浮点数精度的问题**

* 有些数以二进制表示位数是无穷的，只能以一个近似值表示。比如1.1二进制只能用1.09999999999999999表示。所以1.1*3 = 3.3000000000000003 类似的还很多。
* 最简单的解决方法就是先转成整数运算后，再转成小数，也可调用toFixed转换为你需要的精度可避免这个问题。

#### **小程序 rich-text，不支持显示&nbsp空格。需把空格转换为 全角&emsp;/半角**

*  方法：conten.replace(/&(nbsp);/ig, '&ensp;')
*  扩展： &nbsp;（不换行空格）、&ensp;（半角空格）、&emsp;（全角空格）、&thinsp;（窄空格）、&zwnj;（零宽不连字）、&zwj;（零宽连字

#### **新小程序注册配置流程**

* 小程序信息配置-》微信认证-》-服务类目配置-》开发人员配置-》后台需要AppID与AppSecret

####  **小程序mpvue下多层次的数据值改变但是页面不动态渲染**

* 是因为改变的数据是多层级的，没有触发render函数进行自动更新，需要在数据改变以后手动调用this.$forceUpdate();

#### **小程序第一次登陆后再进入，步数不实时更新问题**

* 再次进入页面时，**不需要再调用wx.login**，否则会导致session_key失效，导致获取微信步数时，步数不更新

## PC遇到问题

#### **编辑器之间的简单比**

* UEditor：基本满足各种需求，依赖于jquery但是已经不再维护了，实现上传图片等需要修改源码，界面不太美观，对于老浏览器兼容还不错，但是我这里采用的是VueJS来开发，所以放弃
* wangEditor：比较轻量级，最最最重要的是有中文文档上手快，UI也比较漂亮，而且还是国产的, 对于编辑器功能需求少的兄die可以考虑，但是考虑到我这项目业务比较重，所以只好放弃
* Bootstrap-wysiwyg：简洁好看，依赖于Bootstrap, jquery，选择的Element-ui弃之
  vue-quill-editor：功能太简洁，不支持空格转换&nbsp;.
* TinyMCE: 支持图片在线处理,插件多，功能强。

#### **TinyMCE编辑器使用(vue cli3 + tinymce4 )**

* .官网下载安装包解压,汉化包单独下载，引入public;在index.html中引入主要./tinymce/tinymce.min.js与汉化./tinymce/zh_CN.js;

* 通过  window.tinymce.init 。初始化编辑器各种配置。

* 具体使用：

  ```vue
  <template>
      <div class="tinymce-container editor-container">
          <textarea :id="tinymceId" class="tinymce-textarea" v-model="content" />
      </div>
  </template>
  ```

  ```javascript
  data() {
      return {
          tinymceId: "timymc",
          toolbar: [
              "fontsizeselect fontselect image  bold italic underline strikethrough alignleft aligncenter alignright outdent indent  blockquote hr bullist numlist    searchreplace  fullscreen",
              "undo redo emoticons charmap forecolor backcolor table insertdatetime  preview"
          ],
          plugins: [
              "advlist autolink emoticons charmap advlist anchor autolink autosave code codesample colorpicker colorpicker contextmenu directionality emoticons fullscreen hr image imagetools importcss insertdatetime link lists nonbreaking noneditable pagebreak paste preview print save searchreplace spellchecker tabfocus table template textcolor textpattern visualblocks visualchars wordcount "
          ],
          height: 560,
          menubar: 'file edit insert view format table',
          config: null,
          finishInit: false,
          hasChanged: false,
      }
  },
  // 页面离开销毁组件   
  destroyed() {
      const tinymce = window.tinymce.get(this.tinymceId)
      if (tinymce) {
          tinymce.destroy()
      }
  },
  created() {
      this.config = {
          language: 'zh_CN',
          height: this.height,
          menubar: this.menubar, //菜单:指定应该出现哪些菜单
          toolbar: this.toolbar.length > 0 ? this.toolbar : toolbar, // 分组工具栏控件
          plugins: this.plugins, // 插件(比如: advlist | link | image | preview等)
          object_resizing: false, // 是否禁用表格图片大小调整
          end_container_on_empty_block: true, // enter键 分块
          powerpaste_word_import: 'merge', // 是否保留word粘贴样式  clean | merge
          code_dialog_height: 450, // 代码框高度 、宽度
          code_dialog_width: 1000,
          advlist_bullet_styles: 'square', // 无序列表 有序列表
          plugin_preview_width: 375, // 预览宽度
          plugin_preview_height: 668,
          font_formats: "微软雅黑=微软雅黑,Microsoft YaHei;宋体=宋体,SimSun;黑体=黑体, SimHei;隶书=隶书, SimLi;楷体=楷体,楷体_GB2312, SimKai;andale mono=andale mono;arial=arial, helvetica,sans-serif;arial black=arial black,avant garde;comic sans ms=comic sans ms;impact=impact,chicago;Arial=Arial;Verdana=Verdana;Georgia=Georgia;Times New Roman=Times New Roman;Trebuchet MS=Trebuchet MS;Courier New=Courier New;Impact=Impact;Comic Sans MS=Comic Sans MS;Calibri=Calibri",
          fontsizeFormats: '8pt 9pt 10pt 11pt 12pt 14pt 16pt 18pt 20pt 24pt 36pt 48pt',
          content_style: ` * { padding:0; margin:0; }	img {max-width:100% !important } p{line-height:1.4;margin-bottom:12px;}`,
          paste_data_images: true,// 设置为“true”将允许粘贴图像，而将其设置为“false”将不允许粘贴图像。
          image_advtab: true,
          image_caption: true,
          object_resizing : true//调整图像/表格大小
  
      }
  },
  mounted() {
      //初始化 
      this.initTinymce();
  },
  methods: {
      initTinymce() {
          const self = this
          window.tinymce.init({
              selector: `#${this.tinymceId}`,
              ...this.config,
              content_style: 'img {max-width:100% !important }',
              // 初始化赋值
              init_instance_callback: editor => {
                  if (this.content) {
                      editor.setContent(this.content)
                  }
                  this.finishInit = true
                  editor.on('Blur KeyUp',(el) => {
  
                      this.hasChanged = true
                      let content = editor.getContent();
  
                      // 更新数据
                      self.$emit('onChange',content );
                  })
              },
              // 设置事件
              setup: function(editor) {
                  editor.on('click', function(e) {
  
                  });
              },
              // 上传图片
              images_upload_handler: (blobInfo, fuc, failure) => {
                  const formData = new FormData()
                  formData.append('file', blobInfo.blob())
                  // 使用第三方上传，之后寻找bug后，替换此接口*
                  thirdUpload(formData,res=>{
                      if(res){
                          // 上传图片成功，将自己服务器图片地址 传入fuc函数
                          fuc(res)
                          return;
                      }
                      failure('上传失败')
                  },error=>{
                      failure('上传出错')
                  })
                  
              }
          })
      }
  }
  ```

  

* 切换页面时需要销毁编辑器：  

  ```javascript
  const tinymce = window.tinymce.get(this.tinymceId);
  tinymce.destroy()
  ```

#### **PC预览 word 、Exce、ppt代码**

* 官方网址：https://products.office.com/zh-CN/office-online/view-office-documents-online

#### **预览pdf**

* 下载：npm install --save vue-pdf
* 引入：import pdf from 'vue-pdf'
* 注册 组件 :components:{ pdf }
*   解决跨域： this.pdfsrc = pdf.createLoadingTask(this.pdfsrc)
* 页面使用： <pdf :src="pdfsrc" ></pdf>

#### **修改滚动条样式**

* 参数说明
  ::-webkit-scrollbar 滚动条整体部分
  ::-webkit-scrollbar-thumb  滚动条里面的小方块，能向上向下移动（或往左往右移动，取决于是垂直滚动条还是水平滚动条）
  ::-webkit-scrollbar-track  滚动条的轨道（里面装有Thumb）
  ::-webkit-scrollbar-button 滚动条的轨道的两端按钮，允许通过点击微调小方块的位置。
  ::-webkit-scrollbar-track-piece 内层轨道，滚动条中间部分（除去）
  ::-webkit-scrollbar-corner 边角，即两个滚动条的交汇处
  ::-webkit-resizer 两个滚动条的交汇处上用于通过拖动调整元素大小的小控件

  ```css
  div::-webkit-scrollbar{
    width:10px;
    height:10px;
    /**/
  }
  div::-webkit-scrollbar-track{
    background: rgb(239, 239, 239);
    border-radius:2px;
  }
  div::-webkit-scrollbar-thumb{
    background: #bfbfbf;
    border-radius:10px;
  }
  div::-webkit-scrollbar-thumb:hover{
    background: #333;
  }
  div::-webkit-scrollbar-corner{
    background: #179a16;
  }
  ```

#### **地址栏参数转译**

* .escape()不能直接用于URL编码，它的真正作用是返回一个字符的Unicode编码值。比如"春节"的返回结果是%u6625%u8282，，escape()不对"+"编码 主要用于汉字编码，现在已经不提倡使用
* encodeURI()是Javascript中真正用来对URL编码的函数。 编码整个url地址，但对特殊含义的符号"; / ? : @ & = + $ , #"，也不进行编码。对应的解码函数是：decodeURI()
* encodeURIComponent() 能编码"; / ? : @ & = + $ , #"这些特殊字符。对应的解码函数是decodeURIComponent()
*  假如要传递带&符号的网址，要用encodeURIComponent()

#### **websock 建立长连接**

* 当api为http时，长连接为ws:// ,为https时，长连接为wss://

#### **pc端 px 转 rem**

* cnpm i --save-dev postcss-px2rem

  cnpm i --save-dev  autoprefixer

  cnpm install amfe-flexible --save

* main.js 引入 import 'amfe-flexible';

* vue.config.js

  ```javascript
  const autoprefixer = require('autoprefixer');
  const pxtorem = require('postcss-px2rem');
  
   postcss: {
      plugins: [
        autoprefixer(),
        pxtorem({
          remUnit: 192,
        })
      ]
    }
  ```

#### **引入第三方字体**

* 首先把字体文件（.ttf之类的）放在src/common/font/下；

  接着在font文件夹里面新建一个font.css;

  在文件夹里面放好字体文件（Avinda.ttf)

* 在vue.config.js按照这样配置

  ```javascript
  const path = require('path');
  const utils = {
      assetsPath: function (_path) {
          const assetsSubDirectory = process.env.NODE_ENV === 'production'
              // 生产环境下的 static 路径
              ? 'static'
              // 开发环境下的 static 路径
              : 'static'
              return path.posix.join(assetsSubDirectory, _path)
  
      },
       resolve: function(dir) {
          return path.join(__dirname, '..', dir)
       }
  }
  module.exports = {
      configureWebpack:{
          plugins:[...],
          module:{
              rules:[
              {
                test:/\.(woff2?|eot|ttf|otf)(\?.*)$/,
                loader:'url-loader',
                options:{
                  limit: 10000,
                  name: utils.assetsPath('fonrs/[name].[hash:7].[ext]')
                }
              }
            ]
          }, 
      }
  }
  ```

#### **vue cli3 解决跨域 proxy**

* 在 vue.config.js 做以下配置

  ```javascript
  //让请求的地址与地址栏地址本地地址 同源，真正的地址走代理
  devServer: {
      proxy: {
          '/api': {
              target: '当前地址', //API服务器的地址 http://manage.i31.com/api
              ws: true,  //代理websockets
              changeOrigin: true, // 虚拟的站点需要更管origin
              pathRewrite: {   //重写路径 比如'/api/aaa/ccc'重写为'/aaa/ccc'
                  '^/api': ''
              }
          }
      },
    },
  ```

#### **使用type=file 上传文件时各文档如docx需配置的accept属性值**

```html
.xlsx   application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
.xltx   application/vnd.openxmlformats-officedocument.spreadsheetml.template
.potx   application/vnd.openxmlformats-officedocument.presentationml.template
.ppsx   application/vnd.openxmlformats-officedocument.presentationml.slideshow
.pptx   application/vnd.openxmlformats-officedocument.presentationml.presentation
.sldx   application/vnd.openxmlformats-officedocument.presentationml.slide
.docx   application/vnd.openxmlformats-officedocument.wordprocessingml.document
.dotx   application/vnd.openxmlformats-officedocument.wordprocessingml.template
.xlsm   application/vnd.ms-excel.addin.macroEnabled.12
.xlsb   application/vnd.ms-excel.sheet.binary.macroEnabled.12
```

#### **ECharts 问题**

* 图表被压缩：设置 grid 属性，为0左右

* 中国地图： 需要设置    geo: { // 这个是重点配置区 map: 'china', // 表示中国地图 ……}

* 每个柱状图提示信息不同：设置series-->label-->normal-->formatter属性

* 图表模糊：

  可设置svg this.$echarts.init(this.$refs.echarts_r_4,null, {renderer: 'svg'});

  设置dpi  window.devicePixelRatio = 2;

* 页面大小更改,图表需调用

  ```javascript
  Vue.prototype.$echartsResize = function(ref){
    window.addEventListener("resize",function(){ 
      ref.resize(); 
    });
  }
  ```

#### **大屏适配，使用rem适配，根据1920*1080尺寸，px/100 + rem**

  

```javascript
(function(){
    resizeFuc();
    window.onresize = function(){
        resizeFuc();
    }
})();
function resizeFuc(){
    var deviceWidth = document.documentElement.clientWidth;
    document.documentElement.style.fontSize = deviceWidth/19.2 + 'px';
}

```

## vsCode 问题

#### **修改vsCode侧边栏样式**

* \VSCode\resources\app\out\vs\workbench\workbench.main.css  
    .monaco-workbench .part>.content

#### **VS code中断点调试Vue CLI 3项目**

* 配置vue.config.js
    ```
    module.exports = {
        configureWebpack: {
            devtool: process.env.NODE_ENV === "production" ? '' : 'source-map',
        }
    };
    
    ```

* 配置babel.config.js 文件 
  ```
    module.exports = {
      "env":{
        "development":{
          "sourceMaps":true,
          "retainLines":true, 
        }
      },
      presets: [
        '@vue/app'
      ]
    }
  ```

* 配置launch.json文件 (f5调试时，可选择自动生成，注：url 根据实际端口调整)
  ```
    {
        "version": "0.2.0",
        "configurations": [
            {
                "type": "chrome",
                "request": "launch",
                "name": "vuejs: chrome",
                "url": "http://localhost:1025",
                "webRoot": "${workspaceFolder}/src",
                "breakOnLoad": true,
                "sourceMapPathOverrides": {
                    "webpack:///src/*": "${webRoot}/*",
                    "webpack:///./src/*": "${webRoot}/*"
                }
            },
        ]
    }
  ```

* 启动项目 npm run serve

* F5打开调试

## **vue问题**

#### **v-for 循环出的自定义组件，通过this.$refs无法访问其内在方法 **

* v-for循环中自定的组件，通过ref 取出来的是数组，所以需要通过索引下标取得

## git问题 

#### **git pull 报错：fatal: refusing to merge unrelated histories（拒绝合并不相关的历史）**

* 出现这个问题的最主要原因还是在于本地仓库和远程仓库实际上是独立的两个仓库。假如我之前是直接clone的方式在本地建立起远程github仓库的克隆本地仓库就不会有这问题了。
* git pull origin master --allow-unrelated-histories （该命令可以合并两个独立启动仓库的历史）。


## 其他

#### **萤石音视频二次开发对讲有回音**

* 解决：不能连入同一个局域网内，且设备与pc端要相隔五米

